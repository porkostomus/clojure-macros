(defmacro pf 
  "A convenience macro that pretty prints the last thing output to a file named pf.edn. "
  [] `(pprint *1 (clojure.java.io/writer "pf.edn")))